Source: cloudflare-scrape
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 12),
               dh-python,
               python3-all,
               python3-requests,
               python3-setuptools
Standards-Version: 4.4.1
Homepage: https://github.com/Anorov/cloudflare-scrape
Vcs-Browser: https://gitlab.com/kalilinux/packages/cloudflare-scrape
Vcs-Git: https://gitlab.com/kalilinux/packages/cloudflare-scrape.git
Testsuite: autopkgtest-pkg-python

Package: python3-cfscrape
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Python module to bypass Cloudflare's anti-bot page (Python 3)
 This package contains a simple Python module to bypass Cloudflare's anti-bot
 page (also known as "I'm Under Attack Mode", or IUAM), implemented with
 Requests.
 .
 Due to Cloudflare continually changing and hardening their protection page,
 cloudflare-scrape requires Node.js to solve Javascript challenges. This allows
 the script to easily impersonate a regular web browser without explicitly
 deobfuscating and parsing Cloudflare's Javascript.
 .
 This package installs the library for Python 3.
